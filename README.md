# assignment-3_java_web-api-creation-with-hibernate

PostgresSQL database using Hibernate and exposing it through a Web API.

Database consists of:
- Movie
- Character
- Franchise

Project contains seeding data.

API Functionality:
- Generic CRUD for each model
- Update characters in a movie
- Update movies in a franchise 
- Get all movies in a franchise
- Get all chracters in a movie
- Get all characters in a franchise

Documentation done with Swagger
http://localhost:8080/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config#/

