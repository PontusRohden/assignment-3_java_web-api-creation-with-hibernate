package com.example.assignment3_java_webapicreationwithhibernate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Assignment3JavaWebApiCreationWithHibernateApplication {

    public static void main(String[] args) {
        SpringApplication.run(Assignment3JavaWebApiCreationWithHibernateApplication.class, args);
    }

}
