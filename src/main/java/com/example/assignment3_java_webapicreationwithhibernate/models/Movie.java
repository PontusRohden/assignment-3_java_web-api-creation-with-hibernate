package com.example.assignment3_java_webapicreationwithhibernate.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotBlank
    @Column(name = "movie_title")
    private String movieTitle;

    @NotBlank
    private String genre;

    @NotBlank
    @Column(name = "release_year")
    private int releaseYear;

    @NotBlank
    @Size(min = 2, max = 30)
    private String director;

    @Column(name = "picture_url")
    private String pictureUrl;

    private String trailer;


    // One movie can belong to only one franchise
    @ManyToOne
    @JoinColumn(name = "franchise_id")
    public Franchise franchise;

    @JsonGetter("franchise")
    public String franchise() {
        if (franchise != null) {
            return "/api/franchises/" + franchise.getId();
        } else {
            return null;
        }
    }

    @ManyToMany
    @JoinTable(
            name = "character_movie",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "character_id")}
    )
    public List<MovieCharacter> movieCharacters;

    @JsonGetter("movieCharacters")
    public List<String> movieCharacters() {
        if (movieCharacters == null) {
            return null;
        } else {
            return movieCharacters.stream().
                    map(movieCharacters -> {
                        return "/api/movieCharacters/" + movieCharacters.getId();
                    }).collect(Collectors.toList());
        }
    }


    // Constructor

    public Movie(long id, String movieTitle, String genre, int releaseYear, String director, String pictureUrl, String trailer) {
        this.id = id;
        this.movieTitle = movieTitle;
        this.genre = genre;
        this.releaseYear = releaseYear;
        this.director = director;
        this.pictureUrl = pictureUrl;
        this.trailer = trailer;
    }

    public Movie() {

    }


    // Getters and Setters

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public List<MovieCharacter> getMovieCharacters() {
        return this.movieCharacters;
    }

    public void setMovieCharacters(ArrayList<MovieCharacter> movieCharacters) {
        this.movieCharacters = movieCharacters;
    }

}