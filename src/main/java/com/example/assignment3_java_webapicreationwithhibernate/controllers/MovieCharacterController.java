package com.example.assignment3_java_webapicreationwithhibernate.controllers;

import ch.qos.logback.core.boolex.EvaluationException;
import com.example.assignment3_java_webapicreationwithhibernate.models.Franchise;
import com.example.assignment3_java_webapicreationwithhibernate.models.MovieCharacter;
import com.example.assignment3_java_webapicreationwithhibernate.repositories.MovieCharacterRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/movieCharacters")

public class MovieCharacterController {
    @Autowired
    private MovieCharacterRepository movieCharacterRepository;

    /**
     * Used to fetch all MovieCharacters in the DB.
     *
     * @return A List containing the MovieCharacters in the DB and a HttpStatus message.
     */
    @Operation(summary = "Get all movieCharacters")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the movieCharacters",
                content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = MovieCharacter.class)) }),
            @ApiResponse(responseCode = "404", description = "MovieCharacters not Found",
                content = @Content) })
    @GetMapping()
    public ResponseEntity<List<MovieCharacter>> getAllCharacters() {
        List<MovieCharacter> movieCharacters = movieCharacterRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(movieCharacters, status);
    }

    /**
     * Used to fetch a specific MovieCharacter in the DB.
     *
     * @param id The id of the MovieCharacter we're looking for
     * @return if found: The found MovieCharacter and HttpStatus OK, else an empty MovieCharacter object and
     * HttpStatus NotFound.
     */
    @Operation(summary = "Get a movieCharacter by it's id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the movieCharacter",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieCharacter.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Franchise not found",
                    content = @Content) })
    @GetMapping("/{id}")
    public ResponseEntity<MovieCharacter> getCharacterById(@PathVariable Long id) {
        MovieCharacter movieCharacter = new MovieCharacter();
        HttpStatus status;

        if (movieCharacterRepository.existsById(id)) {
            status = HttpStatus.OK;
            movieCharacter = movieCharacterRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(movieCharacter, status);
    }

    /**
     * Adds a MovieCharacter to the DB.
     *
     * @param moveMovieCharacter The MovieCharacter object we want to add.
     * @return The added MovieCharacter and HttpStatus Created.
     */
    @Operation(summary = "Add a new movieCharacter")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "MovieCharacter successfully added",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieCharacter.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid movieCharacter object supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "movieCharacter not found",
                    content = @Content) })
    @PostMapping()
    public ResponseEntity<MovieCharacter> addMovieCharacter(@RequestBody MovieCharacter moveMovieCharacter) {
        HttpStatus status;
        moveMovieCharacter = movieCharacterRepository.save(moveMovieCharacter);
        status = HttpStatus.CREATED;
        return new ResponseEntity<>(moveMovieCharacter, status);
    }

    /**
     * Updates an existing MovieCharacter in the DB.
     *
     * @param id             The id of the MovieCharacter we want to update.
     * @param movieCharacter A MovieCharacter object containing the new data and the old data of the variables we
     *                       don't want to update.
     * @return if the id parameter doesn't match to MovieCharacters id return empty MovieCharacter object and
     * Httpstatus Bad_Request. else return the updated MovieCharacter and HttpStatus No_Content.
     */
    @Operation(summary = "Update a movieCharacter")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Movie successfully updated",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieCharacter.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid movieCharacter object supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "MovieCharacter not found",
                    content = @Content) })
    @PutMapping("/{id}")
    public ResponseEntity<MovieCharacter> updateMovieCharacter(@PathVariable Long id,
                                                               @RequestBody MovieCharacter movieCharacter) {
        MovieCharacter returnMovieCharacter = new MovieCharacter();
        HttpStatus status;

        if (!id.equals(movieCharacter.getId())) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnMovieCharacter, status);
        }
        returnMovieCharacter = movieCharacterRepository.save(movieCharacter);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnMovieCharacter, status);
    }

    /**
     * Removes a MovieCharacter from the DB
     *
     * @param id the id of the MovieCharacter we want to remove.
     * @return If the MovieCharacter exists, returns nulled MovieCharacter and HttpStatus Ok, else empty MovieCharacter
     * object and HttpStatus Not_Found.
     */
    @Operation(summary = "Delete a movieCharacter")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "MovieCharacter successfully deleted",
                    content = {@Content(mediaType = "application(json",
                            schema = @Schema(implementation = MovieCharacter.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid movieCharacter id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "MovieCharacter not found",
                    content = @Content) })
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteMovieCharacter(@PathVariable Long id) {
        HttpStatus status;
        String returnString;
        if (movieCharacterRepository.existsById(id)) {
            status = HttpStatus.OK;
            returnString = "Character with id: " + id + " was successfully deleted.";
            movieCharacterRepository.deleteById(id);
        } else {
            returnString = "Could not delete character with id: " + id + ".";
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnString, status);
    }
}