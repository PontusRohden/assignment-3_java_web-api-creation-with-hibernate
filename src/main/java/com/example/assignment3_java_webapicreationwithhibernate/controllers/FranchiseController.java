package com.example.assignment3_java_webapicreationwithhibernate.controllers;

import com.example.assignment3_java_webapicreationwithhibernate.models.Franchise;
import com.example.assignment3_java_webapicreationwithhibernate.models.Movie;
import com.example.assignment3_java_webapicreationwithhibernate.models.MovieCharacter;
import com.example.assignment3_java_webapicreationwithhibernate.repositories.FranchiseRepository;
import com.example.assignment3_java_webapicreationwithhibernate.repositories.MovieRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/franchises")

public class FranchiseController {

    @Autowired
    private FranchiseRepository franchiseRepository;

    @Autowired
    private MovieRepository movieRepository;


    /**
     * Used to fetch all Franchises in the DB
     *
     * @return A List containing the franchises in the DB and a HttpStatus message
     */
    @Operation(summary = "Get all franchises")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the Franchises",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Franchise.class)) }),
            @ApiResponse(responseCode = "404", description = "Franchises not found",
                    content = @Content) })
    @GetMapping()
    public ResponseEntity<List<Franchise>> getAllFranchises() {
        List<Franchise> franchises = franchiseRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(franchises, status);
    }


    /**
     * Used to Fetch a specific Franchise in the DB.
     *
     * @param id The id of the Franchise we're looking for.
     * @return If found: the found franchise and HttpStatus OK, else an empty franchise object and HttpStatus NotFound.
     */
    @Operation(summary = "Get a franchise by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the Franchise",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Franchise.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Franchise not found",
                    content = @Content) })
    @GetMapping("/{id}")
    public ResponseEntity<Franchise> getFranchiseById(@Parameter(description = "id of Franchise to be searched") @PathVariable Long id) {
        Franchise franchise = new Franchise();
        HttpStatus status;

        if (franchiseRepository.existsById(id)) {
            status = HttpStatus.OK;
            franchise = franchiseRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(franchise, status);
    }


    /**
     * Used to Fetch all Movies in a specific Franchise from the DB.
     *
     * @param id The id of the Franchise we're looking for.
     * @return If found: A list of Movies and HttpStatus OK, else an empty List and HttpStatus NotFound.
     */
    @Operation(summary = "Get all Movies in a Franchise by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the Movies in the Franchise",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Franchise.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Movies not found",
                    content = @Content) })
    @GetMapping("/{id}/movies")
    public ResponseEntity<List<Movie>> getAllMoviesFromFranchise(@Parameter(description = "id of Franchise to be searched") @PathVariable long id) {
        HttpStatus status;
        List<Movie> movies = null;

        if (franchiseRepository.existsById(id)) {
            status = HttpStatus.OK;
            Franchise franchise = franchiseRepository.findById(id).get();

            movies = franchise.getMovies();

        } else {
            status = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(movies, status);
    }

    /**
     * Used to Fetch all MovieCharacters in a specific Franchise from the DB.
     *
     * @param id The id of the Franchise we're looking for.
     * @return If found: A list of MovieCharacters and HttpStatus OK, else an empty List and HttpStatus NotFound.
     */
    @Operation(summary = "Get all Movie Characters in a Franchise by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the Movie Characters in the Franchise",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Franchise.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Movie Characters not found",
                    content = @Content) })
    @GetMapping("/{id}/movieCharacters")
    public ResponseEntity<List<MovieCharacter>> getAllMovieCharactersFromFranchise(@Parameter(description = "id of Franchise to be searched") @PathVariable long id) {
        HttpStatus status;
        List<MovieCharacter> movieCharacters = new ArrayList<>();

        if (franchiseRepository.existsById(id)) {
            Franchise franchise = franchiseRepository.findById(id).get();
            List<Movie> movies = franchise.getMovies();
            status = HttpStatus.OK;

            for (Movie movie : movies) {
                movieCharacters.addAll(movie.getMovieCharacters());
            }

        } else {
            status = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(movieCharacters, status);
    }


    /**
     * Adds a franchise to the DB.
     *
     * @param franchise The Franchise object we want to add.
     * @return the added Franchise and HttpStatus Created.
     */
    @Operation(summary = "Add a new Franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Franchise successfully added",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Franchise.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid Franchise object supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Franchise not found",
                    content = @Content) })
    @PostMapping()
    public ResponseEntity<Franchise> addFranchise(@Parameter(description = "Franchise to be added") @RequestBody Franchise franchise) {
        HttpStatus status;
        franchise = franchiseRepository.save(franchise);
        status = HttpStatus.CREATED;
        return new ResponseEntity<>(franchise, status);
    }


    /**
     * Updates an existing Franchise in the DB.
     *
     * @param id        The id of the Franchise we want to update.
     * @param franchise A Franchise object containing the new data and the old data of the variables we don't want to update.
     * @return if the id parameter doesn't match to the Franchises id return empty Franchise object and Httpstatus Bad_Request.
     * else return the updated Franchise and HttpStatus No_Content.
     */
    @Operation(summary = "Update a Franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Franchise successfully updated",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Franchise.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid Franchise object supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Franchise not found",
                    content = @Content) })
    @PutMapping("/{id}")
    public ResponseEntity<Franchise> updateFranchiseCharacter(@Parameter(description = "Id of Franchise to be updated & new version of Franchise") @PathVariable Long id, @RequestBody Franchise franchise) {
        Franchise returnFranchise = new Franchise();
        HttpStatus status;

        if (!id.equals(franchise.getId())) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnFranchise, status);
        }
        returnFranchise = franchiseRepository.save(franchise);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnFranchise, status);
    }


    /**
     * Updates the Movies that are in this Franchise.
     *
     * @param id     The id of the Franchise we want to add Movies to.
     * @param movies A list containing the id's of the Movies we want to add.
     * @return if the Franchise exists, returns the updated Franchise object and HttpStatus Ok, else returns empty
     * Franchise object and HttpStatus Not_found.
     */
    @Operation(summary = "Update a Movie in Franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Movie in Franchise successfully updated",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Franchise.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid Franchise id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Franchise not found",
                    content = @Content) })
    @PutMapping("/{id}/setMovies}")
    public ResponseEntity<Franchise> updateMoviesInFranchise(@Parameter(description = "Id of Franchise & list of Movies to update") @PathVariable Long id, @RequestBody List<Long> movies) {

        Franchise franchise = new Franchise();
        HttpStatus status;

        if (franchiseRepository.existsById(id)) {
            status = HttpStatus.OK;
            franchise = franchiseRepository.findById(id).get();
            List<Movie> moviesToAdd = new ArrayList<>();

            for (Long movieId : movies) {
                moviesToAdd.add(movieRepository.getById(movieId));
            }
            franchise.setMovies(new ArrayList<>(moviesToAdd));
            franchise = franchiseRepository.save(franchise);
        } else {
            status = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(franchise, status);
    }


    /**
     * Removes a Franchise from the DB
     *
     * @param id the id of the Franchise we want to remove.
     * @return If the Franchise exists, returns nulled Franchise and HttpStatus Ok, else empty Franchise object and
     * HttpStatus Not_Found.
     */
    @Operation(summary = "Delete a Franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Franchise successfully deleted",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Franchise.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid Franchise id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Franchise not found",
                    content = @Content) })
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteFranchise(@Parameter(description = "Id of Franchise to be deleted") @PathVariable Long id) {
        HttpStatus status;
        String returnString;
        if (franchiseRepository.existsById(id)) {
            status = HttpStatus.OK;
            returnString = "Franchise with id: " + id + " was successfully deleted.";
            franchiseRepository.deleteById(id);
        } else {
            returnString = "Could not delete franchise with id: " + id + ".";
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnString, status);
    }
}
