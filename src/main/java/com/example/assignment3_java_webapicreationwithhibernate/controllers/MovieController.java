package com.example.assignment3_java_webapicreationwithhibernate.controllers;

import com.example.assignment3_java_webapicreationwithhibernate.models.Franchise;
import com.example.assignment3_java_webapicreationwithhibernate.models.Movie;
import com.example.assignment3_java_webapicreationwithhibernate.models.MovieCharacter;
import com.example.assignment3_java_webapicreationwithhibernate.repositories.MovieCharacterRepository;
import com.example.assignment3_java_webapicreationwithhibernate.repositories.MovieRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/movies")
public class MovieController {
    @Autowired
    private MovieRepository movieRepository;
    @Autowired
    private MovieCharacterRepository movieCharacterRepository;

    /**
     * Used to fetch all Movies in the DB.
     *
     * @return A List containing the movies in the DB and a HttpStatus message.
     */
    @Operation(summary = "Get all Movies")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the Movies",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Movie.class)) }),
            @ApiResponse(responseCode = "404", description = "Movies not found",
                    content = @Content) })
    @GetMapping()
    public ResponseEntity<List<Movie>> getAllMovies() {
        List<Movie> movies = movieRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(movies, status);
    }

    /**
     * Used to fetch a specific Movie in the DB.
     *
     * @param id The id of the Movie we're looking for
     * @return if found: The found movie and HttpStatus OK, else an empty movie object and HttpStatus NotFound.
     */
    @Operation(summary = "Get a Movie by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the Movie",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Movie.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Movie not found",
                    content = @Content) })
    @GetMapping("/{id}")
    public ResponseEntity<Movie> getMovieById(@Parameter(description = "id of Movie to be searched") @PathVariable Long id) {
        Movie movie = new Movie();
        HttpStatus status;

        if (movieRepository.existsById(id)) {
            status = HttpStatus.OK;
            movie = movieRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(movie, status);
    }

    /**
     * Used to Fetch all MovieCharacters in a specific Movie from the DB.
     *
     * @param id The id of the Movie we're looking for.
     * @return If found: A list of MovieCharacters and HttpStatus OK, else an empty List and HttpStatus NotFound.
     */
    @Operation(summary = "Get all Characters from a Movie by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the Characters from provided Movie",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Movie.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Characters not found",
                    content = @Content) })
    @GetMapping("/{id}/movieCharacters")
    public ResponseEntity<List<MovieCharacter>> getAllMovieCharactersFromMovie(@Parameter(description = "id of Movie") @PathVariable long id) {
        HttpStatus status;
        List<MovieCharacter> movieCharacters = null;

        if (movieRepository.existsById(id)) {
            status = HttpStatus.OK;
            Movie movie = movieRepository.findById(id).get();

            movieCharacters = movie.getMovieCharacters();

        } else {
            status = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(movieCharacters, status);
    }

    /**
     * Adds a movie to the DB.
     *
     * @param movie The Movie object we want to add.
     * @return The added Movie and HttpStatus Created.
     */
    @Operation(summary = "Add Movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Movie successfully added",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Movie.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid movie supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Movie not found",
                    content = @Content) })
    @PostMapping()
    public ResponseEntity<Movie> addMovie(@Parameter(description = "Movie") @RequestBody Movie movie) {
        HttpStatus status;
        movie = movieRepository.save(movie);
        status = HttpStatus.CREATED;
        return new ResponseEntity<>(movie, status);
    }

    /**
     * Updates an existing Movie in the DB.
     *
     * @param id    The id of the movie we want to update.
     * @param movie A movie object containing the new data and the old data of the variables we don't want to update.
     * @return if the id parameter doesn't match to Movies id return empty Movie object and Httpstatus Bad_Request.
     * else return the updated Movie and HttpStatus No_Content.
     */
    @Operation(summary = "Update Movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Movie successfully updated",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Movie.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid movie supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Movie not found",
                    content = @Content) })
    @PutMapping("/{id}")
    public ResponseEntity<Movie> updateMovie(@Parameter(description = "Id of Movie & Movie") @PathVariable Long id, @RequestBody Movie movie) {
        Movie returnMovie = new Movie();
        HttpStatus status;

        if (!id.equals(movie.getId())) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnMovie, status);
        }
        returnMovie = movieRepository.save(movie);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnMovie, status);
    }

    /**
     * Updates the Characters that are in this Movie.
     *
     * @param id         The id of the Movie we want to add characters to.
     * @param characters A list containing the id's of the characters we want to add.
     * @return if the Movie exists, returns the updated movie object and HttpStatus Ok, else returns empty Movie object
     * and HttpStatus Not_found.
     */
    @Operation(summary = "Update Characters in Movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Characters in Movie successfully updated",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Movie.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid Movie supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Movie not found",
                    content = @Content) })
    @PutMapping("/setCharacters/{id}")
    public ResponseEntity<Movie> updateMovieCharactersInMovie(@Parameter(description = "Id of Movie & list of Characters") @PathVariable Long id, @RequestBody List<Long> characters) {
        Movie movie = new Movie();
        HttpStatus status;

        if (movieRepository.existsById(id)) {
            status = HttpStatus.OK;
            movie = movieRepository.findById(id).get();
            List<MovieCharacter> charactersToAdd = new ArrayList<>();

            for (Long charId : characters) {
                charactersToAdd.add(movieCharacterRepository.getById(charId));
            }

            movie.setMovieCharacters(new ArrayList<>(charactersToAdd));
            movie = movieRepository.save(movie);
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(movie, status);
    }

    /**
     * Removes a movie from the DB
     *
     * @param id the id of the Movie we want to remove.
     * @return If the Movie exists, returns nulled Movie and HttpStatus Ok, else empty Movie object and HttpStatus
     * Not_Found.
     */
    @Operation(summary = "Delete Movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Movie successfully deleted",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Movie.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid Movie supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Movie not found",
                    content = @Content) })
    @DeleteMapping("/{id}")
    public ResponseEntity<String> DeleteMovie(@Parameter(description = "Id of Movie") @PathVariable Long id) {
        HttpStatus status;
        String returnString;
        if (movieRepository.existsById(id)) {
            status = HttpStatus.OK;
            returnString = "Movie with id: " + id + " was successfully deleted.";
            movieRepository.deleteById(id);
        } else {
            returnString = "Could not delete movie with id: " + id + ".";
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnString, status);
    }

}
