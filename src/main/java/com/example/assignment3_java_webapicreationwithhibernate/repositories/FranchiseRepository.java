package com.example.assignment3_java_webapicreationwithhibernate.repositories;

import com.example.assignment3_java_webapicreationwithhibernate.models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FranchiseRepository extends JpaRepository<Franchise, Long> {
}
