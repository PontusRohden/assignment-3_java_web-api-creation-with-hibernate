package com.example.assignment3_java_webapicreationwithhibernate.repositories;

import com.example.assignment3_java_webapicreationwithhibernate.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {
}
